# Change log

All notable changes to this project will be documented in this file.

## [1.0.0] - 2022-07-21
### Added
- Added all basic features of the Taskpost app.

[1.0.0]: https://gitlab.com/CesarACabrera/taskpost-app/-/tags/1.0.0