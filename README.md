# Taskpost

Taskpost es una aplicación de softaware libre que permite planear projectos. 
Cada proyecto contiene una descripción y permite de añadir un costo unitario por tiempo. que se calcula conforme se van añadiendo tareas. En cuanto a las tareas, es posible añadir un tiempo estimado para su desarrollo, fecha de entrega, imágenes, etiquetas y organizarlas por principales y secundarias. También, incluye un buscador de proyectos. Finalmente, permite exportar éstos como imagen JPEG o un archivo Markdown.  Posee un tema oscuro.

Taskpost is a free software application that allows you to plan projects.
Each project contains a description and allows you to add a unit cost per time. which is calculated as tasks are added. As for the tasks, it is possible to add an estimated time for their completition, delivery date, images, labels and organize them by main and secondary. It also includes a project search engine. Finally, it allows you to export these as a JPEG image or Markdown file. It includes a dark theme.

![](./src/assets/screenshots.png)