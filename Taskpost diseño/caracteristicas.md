# Taskpost 1.0.0

## Características

* Añadir proyectos.
* Añadir milestones.
* Añadir fechas para cada milestone (opcional).
* Se debe poder añadir una cuota por hora y calcular al final el presupuesto del proyecto.
* Se debe poder exportar como imagen cada linea.
* La información se debe poder guardar como JSON.
* Se deben poder añadir imágenes a cada milestone.
* Se deben poder añadir descripciones (y se deben poder ocultar).
* Se deben poder borrar los milestones.
* Se deben poder añadir íconos.
* Se deben poder añadir tags.
* Sort milestones por tags.
* Cada milestone debe tener un boton para editar detalles.
* Debe tener un botón de "tarea completada".
* Debe tener tema oscuro.
* La configuración se debe guardar en un JSON.
