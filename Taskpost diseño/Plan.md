* Crear Main.js
* A1 (view)
* A2 (view)
* A1 (lógica)
* B3 (componente)
* Crear/Modificar proyecto (fileSystem)
* Crear/Modificar proyecto (tests)
* B3 (lógica)
* B1 (view)
* B1 (lógica: B1 y B3)
* B1 Menús estáticos (Archivo: [Abrir, Crear, Salir], Ver: [Organizar: [Nombre, Fecha]], Ayuda: [Información])
* B1 Menús estáticos (lógica)
* B1 Menús contextuales (Editar y Borrar)
* Borrar proyecto (fileSystem)
* Borrar proyecto (tests)
* B5 (componente)
* B5 (lógica)
* B4 (lógica)
* C1 (view) // Añadir prioridad de la tarea como radio buttons.
* C4 (componente)
* C4 (lógica)
* C1 (lógica)
* C2
* // C3 y B3 deben ser fusionados.
* C6
* Menús estáticos (Archivo: [Exportar proyecto, Regresar a la página principal], Editar: [Añador tarea, Editar proyecto], Ver: [Organizar: [Fecha, Tags, Tareas: [importantes, Secundarias], Tiempo], Información del proyecto])
* Exportar proyecto (fileSystem)
* Exportar proyecto (test)
* Opciones de exportación (componente)
* Opciones de exportación (lógica)
* Menús estáticos (lógica)
* Menús contextuales (Borrar, Completar)
* Borrar tarea (dialog)
* Menús contextuales (lógica)


