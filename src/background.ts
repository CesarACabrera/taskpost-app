'use strict'

import { app, protocol, BrowserWindow, ipcMain, dialog, Menu, Notification, MenuItem, nativeImage } from 'electron'
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib'
import fs from 'fs'
import installExtension, { VUEJS_DEVTOOLS } from 'electron-devtools-installer'
import { MainContext } from './menus/main/top/mainMenu'
import path from 'path'
import { ProjectContex } from './menus/main/context/projectContextMenu'
import { TaskContext } from './menus/timeline/context/taskContextMenu'
import { TimelineContext } from './menus/timeline/top/timelineMenu'

const isDevelopment = process.env.NODE_ENV !== 'production'
let mainMenu: Menu;
let timelineMenu: Menu;
let win: BrowserWindow;



// Scheme must be registered before the app is ready 
protocol.registerSchemesAsPrivileged([
  { scheme: 'app', privileges: { secure: true, standard: true } }
])

async function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    title: "Taskpost",
    minWidth: 1200,
    minHeight: 810,
    webPreferences: {

      // Use pluginOptions.nodeIntegration, leave this alone
      // See nklayman.github.io/vue-cli-plugin-electron-builder/guide/security.html#node-integration for more info
      nodeIntegration: (process.env
        .ELECTRON_NODE_INTEGRATION as unknown) as boolean,
      contextIsolation: !process.env.ELECTRON_NODE_INTEGRATION,
      preload: path.join(__dirname, 'preload.js')
    },
    icon: "icon.png"
  })

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    await win.loadURL(process.env.WEBPACK_DEV_SERVER_URL as string)
    if (!process.env.IS_TEST) win.webContents.openDevTools()
  } else {
    createProtocol('app')
    // Load the index.html when not in development
    win.loadURL('app://./index.html')
  }

  /*const contextMenu = Menu.buildFromTemplate(template);
  win.webContents.on('context-menu', () => {
    contextMenu.popup();
  })*/
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    try {
      await installExtension(VUEJS_DEVTOOLS)
    } catch (e: any) {
      console.error('Vue Devtools failed to install:', e.toString())
    }
  }
  createWindow()
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', (data) => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}

// Dialog API.

ipcMain.handle('confirmDialog', (event: Electron.IpcMainInvokeEvent, dialogTitle: string, dialogMessage: string, dialogType: string) => {
  const response: number = dialog.showMessageBoxSync({
    message: dialogMessage,
    type: dialogType,
    buttons: ['Cancelar', 'Confirmar'],
    title: dialogTitle
  });

  return response;
});

ipcMain.handle('errorDialog', (event: Electron.IpcMainInvokeEvent, title: string, content: string) => {
  dialog.showErrorBox(title, content);
});

ipcMain.handle('exportProject', (event: Electron.IpcMainInvokeEvent, exportString: string, name: string, mode: string) => {

  let filterType: { name: string, extension: string };

  if (mode === "image") {
    filterType = {
      name: "Imágenes JPEG",
      extension: 'jpeg'
    }
  } else {
    filterType = {
      name: "Archivos Markdown",
      extension: 'md'
    }
  }

  const file = dialog.showSaveDialogSync({
    title: "Exportar proyecto",
    buttonLabel: "Exportar",
    defaultPath: name,
    filters: [{
      name: filterType.name,
      extensions: [filterType.extension]
    }],
    properties: ['showOverwriteConfirmation']
  });

  if (file !== undefined) {
    let toWrite: string | Buffer;
    const filePath: string = file + `.${filterType.extension}`;
    if (mode === "image") {
      toWrite = Buffer.from(exportString, "base64");
    } else {
      toWrite = exportString;
    }
    fs.writeFileSync(filePath, toWrite);

    const exportNotification = new Notification({
      title: "Exportar proyecto",
      body: `Proyecto exportado en: ${filePath}`,
    }).show();
  } else {
    throw new Error("Dialog was cancelled.");
  }
});

ipcMain.handle("openFile", () => {
  const filePath = dialog.showOpenDialogSync({
    title: "Abrir proyecto",
    buttonLabel: "Abrir",
    filters: [{
      name: "Archivos JSON",
      extensions: ['json']
    }],
    properties: ["openFile"]
  });

  return filePath;

});

ipcMain.handle('saveFileAs', (event: Electron.IpcMainInvokeEvent, json: string, name: string) => {
  const file = dialog.showSaveDialogSync({
    title: "Guardar proyecto",
    buttonLabel: "Guardar",
    defaultPath: name,
    filters: [{
      name: "Archivos JSON",
      extensions: ['json']
    }],
    properties: ['showOverwriteConfirmation']
  });

  if (file !== undefined) {
    const filePath: string = file + '.json'
    fs.writeFileSync(filePath, json);

    const saveAsNotification = new Notification({
      title: "Guardar proyecto",
      body: `Proyecto guardado en: ${filePath}`
    }).show();

    return filePath;
  } else {
    throw new Error("Dialog was cancelled.");
  }
});

// FS API.

ipcMain.handle('readFile', (event: Electron.IpcMainInvokeEvent, path: string) => {
  return fs.readFileSync(path, 'utf-8');
});

ipcMain.handle('readPackage', () => {
  const packageJSON = JSON.parse(fs.readFileSync(path.join(__dirname, "package.json"), 'utf-8'));
  return {
    name: packageJSON.name,
    version: packageJSON.version,
    author: packageJSON.author,
    description: packageJSON.description,
    license: packageJSON.license,
    repository: packageJSON.repository
  }
});

ipcMain.handle('saveFile', (event: Electron.IpcMainInvokeEvent, path: string, json: string) => {
  fs.writeFileSync(path, json);
});

// Menu API.

ipcMain.handle("mainMenu", () => {
  const mainContext = new MainContext(win);
  mainMenu = Menu.buildFromTemplate(mainContext.template);

  Menu.setApplicationMenu(mainMenu);
});

ipcMain.handle('projectContextMenu', async () => {
  const projectContext = new ProjectContex();
  const mainContextMenu = Menu.buildFromTemplate(projectContext.template);

  function result(): Promise<string> {
    return new Promise((resolve) => {
      mainContextMenu.popup({
        callback: () => {
          resolve(projectContext.callback);
        }
      });
    });
  }

  return await result();
});

ipcMain.handle('switchEnableMenuItem', (event: Electron.IpcMainInvokeEvent, menu: string, indexArray: number[], mode: boolean) => {
  function disableItem(menu: Menu, indexArray: number[], mode: boolean) {
    const element = menu.items[indexArray.shift() as number];

    if (indexArray.length > 0) {
      disableItem(element.submenu as Menu, indexArray, mode);
    } else {
      element.enabled = mode;
    }
  }

  switch (menu) {
    case "timeline":
      disableItem(timelineMenu, indexArray, mode);
    case "main":
      disableItem(mainMenu, indexArray, mode);
  }
});

ipcMain.handle('taskContextMenu', async () => {
  const taskContext = new TaskContext();
  const contextMenu = Menu.buildFromTemplate(taskContext.template);

  function result(): Promise<string> {
    return new Promise((resolve) => {
      contextMenu.popup({
        callback: () => {
          resolve(taskContext.callback);
        }
      });
    })
  }

  return await result();
});

ipcMain.handle('timelineMenu', () => {
  const timelineContext = new TimelineContext(win);
  timelineMenu = Menu.buildFromTemplate(timelineContext.template);

  Menu.setApplicationMenu(timelineMenu);
});





