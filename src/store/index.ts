import { Project, ProjectData, Tag, Task } from '@/modules/fileSystem'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    currentPath: "" as null | string,
    currentProject: {} as Project,
    currentTag: {} as Tag,
    paths: { ...localStorage },
    previousTitle: "" as string,
    projects: [] as ProjectData[],
    selectedTask: {} as Task
  },
  mutations: {
    addProject(state, payload: ProjectData) {
      state.projects.push(payload);
    },
    clearProjects(state) {
      state.projects = [];
    },
    completeTask(state, payload: number) {
      state.currentProject.tasks[payload].complete = !state.currentProject.tasks[payload].complete;
    },
    deleteProject(state, payload: string) {
      state.projects.splice(state.projects.findIndex((project: ProjectData) => project.title === payload), 1);
    },
    deleteTask(state, payload: number) {
      state.currentProject.tasks.splice(payload, 1);
    },
    getProjects(state) {
      const paths = { ...localStorage };

      for (const project in paths) {
        if (project !== "loglevel:webpack-dev-server" && project !== "darkMode") {
          state.projects.push({
            title: project,
            ...JSON.parse(paths[project]),
          });
        }
      }

    },
    reindex(state) {
      let count = 0;
      state.currentProject.tasks.forEach(task => {
        task.index = count;
        count++;
      })
    },
    setCurrentPath(state, payload: null | string) {
      state.currentPath = payload;
    },
    setCurrentProject(state, payload: Project) {
      state.currentProject = payload;
    },
    setCurrentTag(state, payload: Tag) {
      state.currentTag = payload;
    },
    setPreviousTitle(state, payload: string) {
      state.previousTitle = payload;
    },
    setSelectedTask(state, payload: number) {
      state.selectedTask = state.currentProject.tasks[payload];
    }
  },
  actions: {
    addProject(context, payload: ProjectData) {
      context.commit('addProject', payload);
    },
    completeTask(context, payload: number) {
      context.commit('completeTask', payload);
    },
    deleteProject(context, payload: string) {
      context.commit('deleteProject', payload)
    },
    deleteTask(context, payload: number) {
      context.commit('deleteTask', payload);
      context.commit('reindex');
    },
    getProjects(context) {
      context.commit('clearProjects');
      context.commit('getProjects');
    },
    setCurrentPath(context, payload: null | string) {
      context.commit('setCurrentPath', payload);
    },
    setCurrentProject(context, payload: Project) {
      context.commit('setCurrentProject', payload);
    },
    setCurrentTag(context, payload: Tag) {
      context.commit('setCurrentTag', payload);
    },
    setSelectedTask(context, payload: number) {
      context.commit('setSelectedTask', payload);
    },
    setPreviousTitle(context, payload: string) {
      context.commit('setPreviousTitle', payload);
    }
  }
});