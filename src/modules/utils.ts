export function isAProject(json: any): boolean {
    return 'title' in json && 'description' in json && 'date' in json && 'cost' in json && 'tasks' in json && 'tagList' in json;
}