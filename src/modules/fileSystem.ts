export class Project {
    constructor(
        public title: string,
        public description: string,
        public date: string,
        public cost: cost | null,
        public tasks: Task[],
        public tagList: Tag[]
    ) { }

    createNewTask() {
        this.tasks.push(new Task(
            this.tasks.length,
            "Nueva tarea",
            "Nueva tarea",
            0,
            null,
            new Date().toISOString().split('T')[0],
            [],
            true,
            false,
            false));
    }
}

export class Tag {
    constructor(
        public title: string,
        public color: string,
        public icon: string

    ) { }
}

export class Task {
    constructor(
        public index: number,
        public title: string,
        public description: string,
        public time: number,
        public image: string | null,
        public date: string,
        public tags: Tag[],
        public primary: boolean,
        public complete: boolean,
        public show: boolean
    ) { }
}

export interface AppData {
    name: string,
    version: string,
    author: { name: string, email: string },
    description: string,
    license: string,
    repository: { url: string }
}

export interface ProjectData {
    title: string,
    path: string,
    date: string,
    description: string
}

export type cost = {
    price: number,
    currency: string,
    unit: string
}