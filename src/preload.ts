import { contextBridge, ipcRenderer } from "electron";

contextBridge.exposeInMainWorld("dialogAPI", {
    confirmDialog(dialogTitle: string, dialogMessage: string, dialogType: string) {
        return ipcRenderer.invoke("confirmDialog", dialogTitle, dialogMessage, dialogType);
    },
    errorDialog(title: string, content: string): void {
        ipcRenderer.invoke("errorDialog", title, content);
    },
    exportProject(exportString: string, name: string, mode: string): void {
        ipcRenderer.invoke("exportProject", exportString, name, mode);
    },
    openFile(): Promise<string[] | undefined> {
        return ipcRenderer.invoke("openFile");
    },
    saveFileAs(json: string, name: string): Promise<string> {
        return ipcRenderer.invoke("saveFileAs", json, name);
    }
});


contextBridge.exposeInMainWorld("fsAPI", {
    readFile(path: string): Promise<string> {
        return ipcRenderer.invoke('readFile', path);
    },
    readPackage(): Promise<string> {
        return ipcRenderer.invoke('readPackage');
    },
    saveFile(path: string, json: string): void {
        ipcRenderer.invoke('saveFile', path, json);
    }
});


contextBridge.exposeInMainWorld("menuAPI", {
    mainMenu(): void {
        ipcRenderer.invoke("mainMenu");
    },
    async projectContextMenu(title: string): Promise<string> {
        return await ipcRenderer.invoke("projectContextMenu", title);
    },
    switchEnableMenuItem(menu: string, indexArray: number[], mode: boolean): void {
        ipcRenderer.invoke("switchEnableMenuItem", menu, indexArray, mode);
    },
    async taskContextMenu(index: number): Promise<string> {
        return await ipcRenderer.invoke("taskContextMenu", index);
    },
    timelineMenu(): void {
        ipcRenderer.invoke("timelineMenu");
    },
});

contextBridge.exposeInMainWorld("rendererEventsAPI", {
    mainEvent: (callback: (event: Electron.IpcRendererEvent, ...args: any[]) => void) => ipcRenderer.on('mainEvent', callback),
    removeListeners(component: string): void {
        switch (component) {
            case "timeline":
                ipcRenderer.removeAllListeners('timelineEvent');
                break;
            case "main":
                ipcRenderer.removeAllListeners('mainEvent');
                break;
        }

    },
    timelineEvent: (callback: (event: Electron.IpcRendererEvent, ...args: any[]) => void) => ipcRenderer.on('timelineEvent', callback),
});


