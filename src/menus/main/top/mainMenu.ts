import { BrowserWindow } from "electron";
import { MenuItemConstructorOptions } from "electron/main";

export class MainContext {
    public template: MenuItemConstructorOptions[] = [{
        label: "Archivo",
        submenu: [
            {
                label: "Abrir",
                click: () => this.window.webContents.send("mainEvent", "openProject"),
                accelerator: "CmdOrCtrl+Shift+A"
            },
            {
                label: "Crear",
                click: () => this.window.webContents.send("mainEvent", "createProject"),
                accelerator: "CmdOrCtrl+Shift+C"
            },
            {
                role: "quit"
            }
        ]
    },
    {
        label: "Ver",
        submenu: [
            {
                label: "Organizar",
                submenu: [
                    {
                        label: "Nombre",
                        click: () => this.window.webContents.send("mainEvent", "sortByName")
                    },
                    {
                        label: "Fecha",
                        click: () => this.window.webContents.send("mainEvent", "sortByDate")
                    }
                ]
            },
            {
                label: "Modo claro/oscuro",
                click: () => this.window.webContents.send("mainEvent", "darkMode")
            }
        ]
    },
    {
        label: "Ayuda",
        submenu: [
            {
                label: "Información",
                click: () => this.window.webContents.send("mainEvent", "information")
            }
        ]
    }
    ];

    constructor(private window: BrowserWindow) { }
}