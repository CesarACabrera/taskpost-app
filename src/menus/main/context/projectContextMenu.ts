import { MenuItemConstructorOptions } from "electron/main";

export class ProjectContex {
    public template: MenuItemConstructorOptions[]
    public callback: string

    constructor() {
        this.template = [
            {
                label: "Editar",
                click: this.editProject.bind(this)
            },
            {
                label: "Borrar",
                click: this.deleteProject.bind(this)
            }
        ];
        this.callback = '';

    }

    editProject() {
        this.callback = 'editProject';
    }
    deleteProject() {
        this.callback = 'deleteProject';
    }
}