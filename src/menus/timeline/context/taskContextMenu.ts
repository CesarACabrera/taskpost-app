import { MenuItemConstructorOptions } from "electron/main";

export class TaskContext {
    template: MenuItemConstructorOptions[]
    callback: string

    constructor() {
        this.template = [
            {
                label: 'Eliminar tarea',
                click: this.deleteTask.bind(this)
            },
            {
                label: 'Completar o reanudar',
                click: this.completeTask.bind(this)

            }
        ];
        this.callback = '';
    }

    deleteTask() {
        this.callback = 'deleteTask';
    }
    completeTask() {
        this.callback = 'completeTask';
    }
}