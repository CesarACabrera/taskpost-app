import { BrowserWindow } from "electron";
import { MenuItemConstructorOptions } from "electron/main";

export class TimelineContext {
    public template: MenuItemConstructorOptions[] = [{
        label: 'Archivo',
        submenu: [
            {
                label: 'Guardar proyecto',
                click: () => this.window.webContents.send("timelineEvent", "saveFileRenderer"),
                accelerator: "CmdOrCtrl+G"
            },
            {
                label: 'Guardar como...',
                click: () => this.window.webContents.send("timelineEvent", "saveFileAsRenderer"),
                accelerator: "CmdOrCtrl+Shift+G"
            },
            {
                label: 'Exportar proyecto',
                submenu: [
                    {
                        label: 'Imagen',
                        click: () => this.window.webContents.send("timelineEvent", "exportProject", "image"),
                        accelerator: "CmdOrCtrl+Shift+I"
                    },
                    {
                        label: 'Markdown',
                        click: () => this.window.webContents.send("timelineEvent", "exportProject", "markdown"),
                        accelerator: "CmdOrCtrl+Shift+M"
                    }
                ]
            },
            {
                label: 'Regresar a la página principal',
                click: () => this.window.webContents.send("timelineEvent", "returnToMainPage"),
                accelerator: "CmdOrCtrl+Backspace"
            }
        ]
    },
    {
        label: 'Editar',
        submenu: [
            {
                label: 'Añadir tarea',
                click: () => this.window.webContents.send("timelineEvent", "addNewTask"),
            },
            {
                label: 'Editar proyecto',
                click: () => this.window.webContents.send("timelineEvent", "editProject")
            }
        ]
    },
    {
        label: 'Ver',
        submenu: [
            {
                label: 'Organizar',
                submenu: [
                    {
                        label: 'Fecha',
                        click: () => this.window.webContents.send("timelineEvent", "sortByRenderer", "date")
                    },
                    {
                        label: "Nombre",
                        click: () => this.window.webContents.send("timelineEvent", "sortByRenderer", "name")
                    },
                    {
                        label: 'Etiquetas',
                        click: () => this.window.webContents.send("timelineEvent", "openTagDialog")
                    },
                    {
                        label: 'Tareas',
                        submenu: [
                            {
                                label: 'Importantes',
                                click: () => this.window.webContents.send("timelineEvent", "sortByRenderer", "important")
                            },
                            {
                                label: 'Secundarias',
                                click: () => this.window.webContents.send("timelineEvent", "sortByRenderer", "secondary")
                            },
                            {
                                label: 'Completadas',
                                click: () => this.window.webContents.send("timelineEvent", "sortByRenderer", "complete")
                            },
                            {
                                label: 'Incompletas',
                                click: () => this.window.webContents.send("timelineEvent", "sortByRenderer", "incomplete")
                            }
                        ]
                    },
                    {
                        label: 'Tiempo aproximado',
                        click: () => this.window.webContents.send("timelineEvent", "sortByRenderer", "time")
                    }
                ]
            },
            {
                label: 'Información del proyecto',
                click: () => this.window.webContents.send("timelineEvent", "projectInfo"),
                accelerator: "CmdOrCtrl+I"
            }
        ]
    }];

    constructor(private window: BrowserWindow) { }
}