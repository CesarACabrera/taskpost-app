import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Main from '../views/Main.vue'
import Timeline from '../views/TimelineView.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Main',
    component: Main
  },
  {
    path: '/timeline',
    name: 'Timeline',
    component: Timeline,
    meta: {
      transition: 'slide-left'
    }
  }
]

const router = new VueRouter({
  routes
})

export default router
