module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  pluginOptions: {
    electronBuilder: {
      nodeIntegration: false,
      preload: 'src/preload.ts',
      builderOptions: {
        appId: "com.electron.taskpost",
        productName: "Taskpost",
        copyright: "Copyright © 2022 Cesar Cabrera",
        linux: {
          target: "AppImage",
          category: "Office",
          synopsis: "Aplicación para la planificación de projectos de desarrollo web.",
          maintainer: "Cesar Cabrera"
        }
      }
    }
  }
}