import { assert } from "chai";
import { isAProject } from "@/modules/utils";

describe("Utils", () => {
    it("isAProject should check if a JSON follows the project strcture or not.", () => {
        const randomJSON = JSON.parse('{"test": "invalid JSON"}');
        const projectJSON = JSON.parse('{"title":"Nuevo Proyecto","description":"Proyecto de Taskpost","date":"2022-07-07","cost":{"price":0,"currency":"","unit":""},"tasks":[{"index":0,"title":"Nueva tarea","description":"Nueva tarea","time":0,"image":null,"date":"2022-07-07","tags":[],"primary":true,"complete":false,"show":false}],"tagList":[]}');

        const validResult = isAProject(projectJSON);
        const invalidResult = isAProject(randomJSON);

        assert.equal(validResult && !invalidResult, true);
    })
})